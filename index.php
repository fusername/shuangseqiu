<?php
	set_time_limit(60000);
	
	$pdo = new PDO('mysql:host=localhost;dbname=shuangseqiu','root','root');
	
	$num = empty($_GET['num']) ? 5000 : $_GET['num'];
	$row = $pdo->query("SELECT COUNT(id) AS numTotal FROM kaijiang")->fetch(PDO::FETCH_ASSOC);
	$numTotal = $row['numTotal'];
	$times = ceil($numTotal / $num);
	$charts = array();
	for($time=0;$time<$times;++$time){
		$red = array();
		$blue = array();
		for($r=1;$r<=33;++$r){
			$rr = $r < 10 ? '0'.$r : ''.$r;
			$sql = "SELECT COUNT(id) AS numTotal FROM (SELECT * FROM kaijiang ORDER BY kjrq DESC LIMIT ".$time*$num.",$num) AS t WHERE CONCAT(',',red1,',',red2,',',red3,',',red4,',',red5,',',red6,',') REGEXP ',$rr,'";
			$row = $pdo->query($sql)->fetch(PDO::FETCH_ASSOC);
			$red[$r] = $row['numTotal'];
		}
		for($b=1;$b<=16;++$b){
			$bb = $b < 10 ? '0'.$b : ''.$b;
			$sql = "SELECT COUNT(id) AS numTotal FROM (SELECT * FROM kaijiang ORDER BY kjrq DESC LIMIT ".$time*$num.",$num) AS t WHERE blue = '$bb'";
			$row = $pdo->query($sql)->fetch(PDO::FETCH_ASSOC);
			$blue[$b] = $row['numTotal'];
		}
		$charts[] = array('red'=>$red,'blue'=>$blue,'label'=>($time*$num).'-'.min($numTotal,$num+$time*$num));
		//echo "SELECT * FROM kaijiang ORDER BY kjrq DESC LIMIT ".$time*$num.",$num".'<br />';
	}
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<title></title>
	<!--[if lte IE 8]><script type="text/javascript" src="cssjs/excanvas.min.js"></script><![endif]-->
	<script type="text/javascript" src="cssjs/jquery.min.js"></script>
	<script type="text/javascript" src="cssjs/jquery.flot.min.js"></script>
</head>
<body>
<div id="redchart" style="height:300px;width:1300px;margin-bottom:10px;"></div>
<div id="bluechart" style="height:300px;width:641px;"></div>
<script type="text/javascript">
	var reds = [],blues = [];
	<?php
		foreach($charts as $chart){
			echo 'reds.push({"label":"'.$chart['label'].'","data":[';
			$str = '';
			foreach($chart['red'] as $k=>$v){
				$str .= "[$k,$v],";
			}
			echo substr($str,0,-1);
			echo ']});';
			
			echo 'blues.push({"label":"'.$chart['label'].'","data":[';
			$str = '';
			foreach($chart['blue'] as $k=>$v){
				$str .= "[$k,$v],";
			}
			echo substr($str,0,-1);
			echo ']});';
		}
	?>
	var options = {
        series: {
			lines: {
				show: true
			},
			points: {
				show: true
			}
		},
		xaxis: {
			tickSize: 1, // 坐标间隔
			tickDecimals: 0, // 没有小数
			labelWidth: 60  // 纵坐标宽度
		},
		legend: {
			noColumns: reds.length
		},
		grid: {
			hoverable: true,
			clickable: true
		},
		hooks: {
			draw: [draw_label]
		}
	};
	$("#redchart").plot(reds,options);
	$("#bluechart").plot(blues,options);
	
	var previousPoint = null;
	$("#redchart,#bluechart").bind("plothover", function (event, pos, item) {
		if(item) {
			if(previousPoint != item.dataIndex) {

				previousPoint = item.dataIndex;

				$("#tooltip").remove();
				var x = item.datapoint[0].toFixed(2),
					y = item.datapoint[1].toFixed(2);

				showTooltip(item.pageX, item.pageY, item.series.label+": "+parseInt(x) + " = " + parseInt(y),item.series.color);
			}
		} else {
			$("#tooltip").remove();
			previousPoint = null;
		}
	});

	$("#redchart,#bluechart").bind("plotclick", function (event, pos, item) {
		if(item) {
			$(this).data("plot").highlight(item.series, item.datapoint);
		}
	});
	
	function showTooltip(x, y, contents, color) {
		var text_color = get_anti_color(color);
		$("<div id='tooltip'>" + contents + "</div>").css({
			position: "absolute",
			display: "none",
			top: y + 15,
			left: x + 15,
			border: "1px solid " + color,
			padding: "2px",
			"background-color": get_rgba_color(color,0.9),
			color: text_color
		}).appendTo("body").fadeIn(200);
	}
	
	function draw_label(plot, canvascontext) {
		//iterate plot items and get the label
		var dtPlot = plot.getData();
		var ctx = canvascontext;
		var item_count = dtPlot.length;
		for(var i = 0; i < item_count; i++) {
			ctx.fillStyle = dtPlot[i].color;
			var plot_item = dtPlot[i];
			var data_count = plot_item.data.length;
			for(var j = 0; j < data_count; j++) {

				var dt = plot_item.data[j];
				var x = dt[0];
				var y = dt[1];
				var data_label = dt[1];
				
				var xAxis = plot.getAxes().xaxis;
				var yAxis = plot.getAxes().yaxis;
				var x_pos = xAxis.p2c(x) + plot.getPlotOffset().left + 15;
				var y_pos = yAxis.p2c(y) + plot.getPlotOffset().top + 4;

				ctx.save();
				ctx.translate(x_pos, y_pos);
				ctx.textAlign = "center";
				ctx.font = "10px tahoma,arial,verdana,sans-serif";
				ctx.fillText(data_label, 0, 0);
				ctx.restore();
			}
		}
	}
	
	function get_anti_color(color){
		var rgb = color.match(/([\d]+)/g);
		var rgb_new = [];
		$.each(rgb,function(i,v){
			rgb_new.push(Math.max(255-v-100,0));
		});
		return "rgb("+rgb_new.join(",")+")";
	}
	function get_rgba_color(color,opacity){
		var rgb = color.match(/([\d]+)/g);
		rgb.push(opacity);
		return "rgba("+rgb.join(",")+")";
	}
</script>
</body>
</html>